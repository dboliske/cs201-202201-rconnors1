// Raymond Connors - CS 201 - Final Exam (04/29/22)

package exams.second;

public class Rectangle extends Polygon { //identifies Rectangle as extending Polygon
	
	private double width; //initialize variables
	private double height;
	
	public Rectangle() { //sets default values for Rectangle
		width = 1;
		height = 1;
	}
	
	public Rectangle(String name, double width, double height) { //assigns which variables are used in Rectangle class
		this.name = name;
		setWidth(width);
		setHeight(height);
	}
	
	public void setWidth(double width) { //sets value of width and ensures it is positive
		if (width > 0) {
			this.width = width;
		}
	}
	
	public void setHeight(double height) { //sets value of height and ensures it is positive
		if (height > 0) {
			this.height = height;
		}
	}
	
	public double getWidth() { //returns value of width
		return width;
	}
	
	public double getHeight() { //returns value of height
		return height;
	}
	
	public String toString() { //toString method to display text below with variable values
		return "Name of shape: " + name + " Width: " + width + "Height: " + height;
	}
	
	public double area() { //compute area of rectangle and return value
		return height * width;
	}
	
	public double perimeter() { //compute perimeter of rectangle and return value
		return 2 * (height + width);
	}
}