// Raymond Connors - CS 201 - Final Exam (04/29/22)

package exams.second;

public class ComputerLab extends Classroom { //ComputerLab described as extending Classroom
	
	private boolean computers; //set boolean for presence (or lack of) computers
	
	public ComputerLab() { //initialize variables
		computers = true;
	}
	
	public ComputerLab(String building, String roomNumber, int seats, boolean computer) { //sets which variables are used in class
		super(building, roomNumber, seats);
		computers = computer;
	}
	
	public void setComputers(boolean computers) { //method for setting variable computers
		this.computers = computers;
	}
	
	public boolean hasComputers() { //returns value of variable computers
		return computers;
	}
	
	public String toString() { //toString method to print text below
		return "Lab is in building " + building + " located in room " + roomNumber + " With computers? " + computers;
	}
}