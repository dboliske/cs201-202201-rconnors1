// Raymond Connors - CS 201 - Final Exam (04/29/22)

package exams.second;

public class Classroom {
	
	protected String building; //initialize all variables
	protected String roomNumber;
	private int seats;
	
	public Classroom() { //create default constructor
		building = "none";
		roomNumber = "0";
		seats = 0;
	}
	
	public Classroom (String building, String roomNumber, int seats) { //create non-default constructor
		this.building = building;
		this.roomNumber = roomNumber;
		this.seats = seats;
	}
	
	public void setBuilding(String building) { //method to obtain value for building variable
		this.building = building;
	}
	
	public void setRoomNumber(String roomNumber) { //method to obtain value for roomNumber variable
		this.roomNumber = roomNumber;
	}
	
	public void setSeats(int seats) { //method to obtain value for seats variable
		if (seats > 0) {
			this.seats = seats;
		}
	}
	
	public String getBuilding() { //method to obtain/return value of building
		return building;
	}
	
	public String getRoomNumber() { //method to obtain/return value of roomNumber
		return roomNumber;
	}
	
	public int getSeats() { //method to obtain/return value of seats
		return seats;
	}
	
	public String toString() { //toString method to print following text
		return "Building is: " + building + " Located in room number: " + roomNumber + " which has " + seats + " available.";
	}
}