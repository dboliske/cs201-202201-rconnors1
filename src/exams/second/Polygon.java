// Raymond Connors - CS 201 - Final Exam (04/29/22)

package exams.second;

public abstract class Polygon {
	
	protected String name; //initialize String variable
	
	public Polygon() { //set default value
		name = "none";
	}
	
	public String getName() { //obtain result of string
		return name;
	}
	
	public void setName(String name) { //set value for variable string
		this.name = name;
	}
	
	public String toString() { //toString method to print text below
		return "Polygon: " + name;
	}
	
	public abstract double area(); //initialize both abstract double variables to be used in extended classes
	public abstract double perimeter();
}