# Final Exam

## Total

97/100

## Break Down

1. Inheritance/Polymorphism:    18/20
    - Superclass:               5/5
    - Subclass:                 5/5
    - Variables:                5/5
    - Methods:                  3/5
2. Abstract Classes:            20/20
    - Superclass:               5/5
    - Subclasses:               5/5
    - Variables:                5/5
    - Methods:                  5/5
3. ArrayLists:                  19/20
    - Compiles:                 5/5
    - ArrayList:                5/5
    - Exits:                    5/5
    - Results:                  4/5
4. Sorting Algorithms:          20/20
    - Compiles:                 5/5
    - Selection Sort:           10/10
    - Results:                  5/5
5. Searching Algorithms:        20/20
    - Compiles:                 5/5
    - Jump Search:              10/10
    - Results:                  5/5

## Comments

1. Constructor allows for non-postive number of seats, allows zero seats (non-postive)
2.
3. Error on non-number input
4.
5.
