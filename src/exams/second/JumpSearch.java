// Raymond Connors - CS 201 - Final Exam (04/29/22)

package exams.second;

import java.util.Scanner;

public class JumpSearch {

	public static void main(String[] args) {
		
		double[] list = new double[] {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142}; //Array given in problem
		Scanner input1 = new Scanner(System.in);
		System.out.println("Enter number to be searched "); //prompt for user input
		double val = Double.parseDouble(input1.nextLine());
		int position = jumpSearch(list, val);
		if (position == -1) {
			System.out.println("-1"); //case of value not present
		} else {
			System.out.println(val + " can be found at " + position); //displays number searched for and its location (if found)
		}
		input1.close();
	}

	public static int jumpSearch(double[] array, double value) { //search routine
		int init = 0;
		int jump = (int) Math.sqrt(array.length);
		while (array[Math.min(jump, array.length) - 1] < value) { //loop to iterate until number is found or all iterations completed
			init = jump;
			jump += (int) Math.sqrt(array.length);
			if (init >= array.length) {
				return -1;
			}
		}
		
		while (array[init] < value) { //do iteration method for entirety of list array
			init++;
			if (init == Math.min(jump, array.length)) {
				return -1;
			}
		}
		
		if (array[init] == value) {
			return init;
		}
	
		return -1;
	}
}