// Raymond Connors - CS 201 - Final Exam (04/29/22)

package exams.second;

import java.util.Scanner;
import java.util.ArrayList;

public class ArrayLists {
	
	public static void main(String[] args) {
		
		boolean done = false; //initialize boolean variable
		
		Scanner input = new Scanner(System.in); //open scanner for user input
		
		ArrayList<String> Array1 = new ArrayList<String>(); //initialize ArrayList
		
		do { //Loop to prompt for user values to be stored in array
			System.out.println("Enter number to be added to array: ");
			String n = input.nextLine();
			Array1.add(n);
			if (n.equalsIgnoreCase("done")) {
				done = true;
			}
		}
		
		while (!done);
		
		ArrayList <Integer> values = new ArrayList<Integer>();
		
		 for (int i = 0; i < Array1.size() - 1; i++) { //adds entered values to array
			 values.add(Integer.parseInt(Array1.get(i)));
		 }
		 
		 int minimum = values.get(0); //finds minimum value entered
		 for (int i = 0; i < Array1.size() - 1; i++) {
			 if(values.get(i) < minimum) {
				 minimum = values.get(i);
			 }
		 }
		 
		 int maximum = values.get(0); //finds maximum value entered
		 for (int i = 0; i < Array1.size() - 1; i++) {
			 if(values.get(i) > maximum) {
				 maximum = values.get(i);
			 }
		 }
		 
		 System.out.println("Minimum: " + minimum + " Maximum: " + maximum); //prints minimum and maximum values from user input
		 
		 input.close(); //close scanner
	}
}
