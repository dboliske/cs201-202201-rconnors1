// Raymond Connors - CS 201 - Final Exam (04/29/22)

package exams.second;

public class Circle extends Polygon { //identifies Circle as extending Polygon
	
	private double radius; //initialize variables
	
	public Circle() { //sets default values for Circle
		radius = 1;
	}
	
	public Circle(String name, double radius) { //assigns which variables are used in Circle class
		this.name = name;
		setRadius(radius);
	}
	
	public void setRadius(double radius) { //sets value of radius and ensures it is positive
		if (radius > 0) {
			this.radius = radius;
		}
	}
	
	public double getRadius() { //returns value of radius
		return radius;
	}
	
	public String toString() { //toString method to display text below with variable values
		return "Name of shape: " + name + " Radius: " + radius;
	}
	
	public double area() { //compute area of circle and return value
		return Math.PI * radius * radius;
	}
	
	public double perimeter() { //compute perimeter of circle and return value
		return 2 * Math.PI * radius;
	}
}