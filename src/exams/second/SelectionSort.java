// Raymond Connors - CS 201 - Final Exam (04/29/22)

package exams.second;

public class SelectionSort {

	public static String[] selectionSort(String[] array1) { //selection sort routine
		for (int i = 0; i < array1.length - 1; i++) {
			int minimum = i;
			for (int j = i + 1; j < array1.length; j++) {
				if(array1[j].compareTo(array1[minimum]) <0) {
					minimum = j;
				}
			}
			
			if (minimum != i) { //switches current item with minimum
				String var1 = array1[i];
				array1[i] = array1[minimum];
				array1[minimum] = var1;
			}
		}
		
		return array1;
	}
	
	public static void main(String[] args) { //using selection sort on given list of strings given
		
		String[] list = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
		
		String[] sortedList = selectionSort(list);
		
		for (int i = 0; i < sortedList.length; i++) { //repeat selection sort for each item within the array
			System.out.println(sortedList[i]);
		}
	}
}