// Raymond Connors - CS201 - Section 02 - 02/25/22 - Question 1

package Exam1;

import java.util.Scanner;

public class Question_1 {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in); //create scanner of input
		
		System.out.print("Enter an integer: "); //prompt for integer
		float given = Float.parseFloat(input.nextLine()); //assign input to a variable (currently float datatype)
		input.close(); //close scanner from further input
		
		int summed = (int) given + 65; //convert float to integer (rounds down to integer if decimal is input) , and add 65 to value
		
		char converted = (char) summed; //convert summed value to character datatype
		
		System.out.println("Given integer (" + (int) given + ") -> added 65 (" + summed + ") -> converted to character: " + converted); //display calculated/converted value

	}

}