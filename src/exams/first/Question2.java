// Raymond Connors - CS201 - Section 02 - 02/25/22 - Question 2

package Exam1;

import java.util.Scanner;

public class Question_2 {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in); //create scanner of input
		
		System.out.print("Enter an integer: "); //prompt for integer
		float given = Float.parseFloat(input.nextLine()); //assign input to a variable (currently float datatype)
		input.close(); //close scanner from further input
		
		int checkdivisibility = (int) given; //convert float to integer (rounds down to integer if decimal is input) , and add 65 to value
		
		if (checkdivisibility % 6==0) {
			System.out.println("foobar");
		}
		else if (checkdivisibility % 2==0) {
			System.out.println("foo");
		}
		else if (checkdivisibility % 3==0) {
			System.out.println("bar");
		}

	}

}
