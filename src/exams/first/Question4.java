// Raymond Connors - CS201 - Section 02 - 02/25/22 - Question 4

package Exam1;

import java.util.Scanner;

public class Question_4 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in); //create scanner for user input
		
		String str[] = new String[10]; //generate string array of desired size (5)
		System.out.println("Enter 5 words you would like compared "); //prompt for user input
		
		for(int i = 0; i < 5; i++) { //allows for user input to be taken 5 times
			String s = input.nextLine(); //resolves input as a string
			str[i] = s; //assigns variable to its location in array
		}
		for (int i = 0; i < 5; i++) { //allows for repetition of array element display for each variable
			System.out.println(str[i] + " "); //prints each element of array
		}
		input.close(); //close scanner from further input
		
		str[5] = " "; //entering empty values into array (to avoid null error I kept getting)
		str[6] = " ";
		str[7] = " ";
		str[8] = " ";
		str[9] = " ";
		
		System.out.println("Duplicate values (if any) are as follows: "); //separate duplicates from printed array (organization)
		
		for (int i = 0; i < 5; i++) { //for-loop to check for duplicate values
			if (str[i].equals(str[i+1])) { //each if-statement checks for equivalence between string values in array
				System.out.println(str[i]); //if duplicate is found, it is printed
			}else if (str[i].equals(str[i+2])) {
				System.out.println(str[i]);
			}else if (str[i].equals(str[i+3])) {
				System.out.println(str[i]);
			}else if (str[i].equals(str[i+4])) {
				System.out.println(str[i]);
			}else if (str[i+1].equals(str[i+2])) {
				System.out.println(str[i+1]);
			}else if (str[i+1].equals(str[i+3])) {
				System.out.println(str[i+1]);
			}else if (str[i+1].equals(str[i+4])) {
				System.out.println(str[i+1]);
			}else if (str[i+2].equals(str[i+3])) {
				System.out.println(str[i+2]);
			}else if (str[i+2].equals(str[i+4])) {
				System.out.println(str[i+2]);
			}else if (str[i+3].equals(str[i+4])) {
				System.out.println(str[i+3]);
			}
		}
	}
}

//apologies for the 'sloppy' way of doing it, kept getting errors when trying to implement for & while loops, 
//so decided to brute force instead. Still works as intended, however.