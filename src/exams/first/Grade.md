# Midterm Exam

## Total

80/100

## Break Down

1. Data Types:                  17/20
    - Compiles:                 4/5
    - Input:                    3/5
    - Data Types:               5/5
    - Results:                  5/5
2. Selection:                   17/20
    - Compiles:                 4/5
    - Selection Structure:      10/10
    - Results:                  3/5
3. Repetition:                  15/20
    - Compiles:                 4/5
    - Repetition Structure:     5/5
    - Returns:                  3/5
    - Formatting:               3/5
4. Arrays:                      19/20
    - Compiles:                 4/5
    - Array:                    5/5
    - Exit:                     5/5
    - Results:                  5/5
5. Objects:                     12/20
    - Variables:                5/5
    - Constructors:             2/5
    - Accessors and Mutators:   3/5
    - toString and equals:      2/5

## Comments

1. Code is good except that it doesn't validate the user input and the class name doesn't match the file name, preventing it from running. (Packages also don't match, but I'm not taking off points for that.)
2. Same issues as in question 1.
3. Same issues as questions 1 and 2 and the triangle format is reversed.
4. Same issue with the class name, but the code itself is much better, though I'm not sure why you made the length of the array 10 instead of 5.
5. Class name is wrong, non-default constructor doesn't use the parameters to initialize instance variables, none-default constructor and mutator do not validate `age`, and `equals` method doesn't follow the UML diagram in taking an Object parameter nor does it compare `names` correctly.
