// Raymond Connors - CS201 - Section 02 - 02/25/22 - Question 3

package Exam1;

import java.util.Scanner;

public class Question_3 {

	public static void main(String[] args) {
		System.out.println("Enter the dimensions of box to be generated: "); //prompt for user input
		Scanner in = new Scanner(System.in); //scan for dimensions to be used
		int n = in.nextInt(); // assign value to variable
		for(int i = n; i > -1; --i) { //create loop for x dimension
			for(int j = n; j > 0; --j) { //create loop for y dimension
				System.out.print(" "); //print empty spaces
				}
			for (int k = 0; k <= i-1; k++) { //loop toprevent filling-in of entire box
				System.out.print(" *"); //print asterisk-filled spaces
			}
			System.out.println(); //creates new line for each successive line of asterisks
		}
		in.close(); //close scanner from further input
	}
}
//struggled to reverse direction to exactly mimic what was described in exam question
//triangle angles diagonally upwards instead of downwards, still follows guidelines though