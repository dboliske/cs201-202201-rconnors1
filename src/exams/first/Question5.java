// Raymond Connors - CS201 - Section 02 - 02/25/22 - Question 5

package Exam1;

public class Question_5 { //sets name of class (named to follow question format, i know it should be Pet()
	private String name; //defines variable 'name' as a String
	private int age; //defines variable 'age' as an integer
	
	public Question_5() { //set default values
		name = "no-name";
		age = 0;
	}
	
	public Question_5(String name, int age) { //set up constructor
		name = "none";
		age = 0;
	}
	
	public void setName (String name) { //mutator method implementation for variable 'name'
		this.name = name;
	}
	
	public void setAge (int age) { //mutator method implementation for variable 'age'
		this.age = age;
	}
	
	public String getName() { //accessor method implementation for variable 'name'
		return name;
	}
	
	public int getAge() { //accessor method implementation for variable 'age'
		return age;
	}
	
	public boolean equals(Question_5 Q) { //checks for equal instances of the class
		if (this.name != Q.getName()) { //compares value of name
			return false;
		} else if (this.age != Q.getAge()) { //compares value of age
			return false;
		}
		return true;
	}
	
	public String toString() { //toString method to describe as string
		return name + " - " + age;
	}
}