// Raymond Connors - CS201 - Section 02 - 01/19/22 - Name + Birthdate Program
package labs.lab0;

public class NameBirthdate {

	public static void main(String[] args) {
		String name = "Raymond Connors";
		String month = "Apr.";
		byte day = 25;
		short year = 2000;
		
		System.out.println("My name is " + name + " and my birthdate is " + month + " " + day + ", " + year + ".");

	}

}