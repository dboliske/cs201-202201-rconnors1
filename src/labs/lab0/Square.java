// Raymond Connors - CS201 - Section 02 - 01/19/22 - Square Program
// Make top of square using "-" like this -> ---------
// Make sides of square using "|" separated by spaces like this -> |           |
// Make bottom of square using same method as top of square
// Print the corresponding parts of the square through variable usage
package labs.lab0;

public class Square {

	public static void main(String[] args) {
		String top = " -----------";
		String middle = "|           |";
		System.out.println(top);
		System.out.println(middle);
		System.out.println(middle);
		System.out.println(middle);
		System.out.println(middle);
		System.out.println(top);

	}

}