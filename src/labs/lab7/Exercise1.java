// Raymond Connors - CS201 - Section 02 - 04/04/22 - Exercise 1

package labs.lab7;

public class Exercise1 {

	public static void bubbleSort(int[] arr) {
		int n = arr.length; //find length of array (10 in this case)
		int temp = 0;
		  
		for(int i = 0; i < n; i++) { //initialize loop
			for(int j=1; j < (n-i); j++) { 
				if(arr[j-1] > arr[j]) { //comparison of element in relation to its neighbors
					temp = arr[j-1]; //assigns element a temporary location  
					arr[j-1] = arr[j]; //replace element in new position
					arr[j] = temp; //replace element whose position was taken during the relocation
				}
			}
		}
	}
	public static void main(String[] args) {
		int arr[] = { 10, 4, 7, 3, 8, 6, 1, 2, 5, 9 }; //GIVEN ARRAY
		System.out.println("Base array (as given:)");

		for(int i = 0; i < arr.length; i++) { //prints each element within initial array
			System.out.print(arr[i] + " ");
		}
		System.out.println();
		bubbleSort(arr); //calls bubblesort routine
		System.out.println("Sorted Array");

		for(int i = 0; i < arr.length; i++) { //prints sorted array
			System.out.print(arr[i] + " ");
		}
	}
}