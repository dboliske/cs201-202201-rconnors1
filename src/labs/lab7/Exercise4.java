package labs.lab7;

import java.util.Scanner;

public class Exercise4 {
	static int BinarySearch(String[] arr, String n){ //comparison of string value to array elements
		int l = 0, r = arr.length - 1;
		while (l <= r) {
			int m = l + (r - l) / 2;
			int res = n.compareTo(arr[m]);
			
			if (res == 0)
				return m;
			if (res > 0)
				l = m + 1;
			else  
				r = m - 1;
			}
		return -1;
		}
	
	public static void main(String []args){
		String[] arr = { "c", "html", "java", "python", "ruby", "scala"}; //GIVEN ARRAY
		Scanner sc= new Scanner(System.in); //open scanner for user input
		System.out.print("Enter a string: "); //prompt for string entry
		String str= sc.nextLine(); //store input value
		int result = BinarySearch(arr, str); //search for input value
		if (result == -1)
			System.out.println("Element not found"); //case if element is not within array
		else
			System.out.println("Element found! Located at " + "index " + result); //case if element is within array
		}
	}