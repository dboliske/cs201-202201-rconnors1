// Raymond Connors - CS201 - Section 02 - 04/04/22 - Exercise 2

package labs.lab7;

public class Exercise2 {
	public static void main(String[] args) {
	       String[] arr ={"cat", "fat", "dog", "apple", "bat", "egg"}; //GIVEN ARRAY
	       int count = 0;
	       String sortedArray[] = insertion(arr, arr.length); //determine length of array
	       for(int i=0;i<sortedArray.length;i++){
	    	   System.out.println(sortedArray[i]); //prints each element of sorted array
	       }
	}

	public static String[] insertion(String array[], int n){
		String temp=""; //empty temp value
		for(int i=0;i<n;i++){ //initialize loop
			for(int j=i+1;j<n;j++){
				if(array[i].compareToIgnoreCase(array[j])>0){ //create conditional 
					temp = array[i]; //if conditional is met, store element in temp location
					array[i]=array[j]; //replace with value from temp location
					array[j]=temp; //return replaced value to position of swapped value
				}
			}
		}
		return array; //return sorted array
	}
}