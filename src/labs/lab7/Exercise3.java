// Raymond Connors - CS201 - Section 02 - 04/04/22 - Exercise 3

package labs.lab7;

class Exercise3
{
    public void selection(double arr[])
    {
        int n = arr.length;
        for (int i = 0; i < n-1; i++){ //loop to determine smallest element
            int min = i; //initialize min variable
            for (int j = i+1; j < n; j++)
                if (arr[j] < arr[min])
                    min = j; //assignment of smallest element to variable
            double temp = arr[min]; //process of switching locations of smallest element with temp
            arr[min] = arr[i];
            arr[i] = temp;
        }
    }
  
    public void printArray(double arr[]){
        int n = arr.length;
        for (int i=0; i<n; ++i)
            System.out.print(arr[i]+" ");
        System.out.println();
    }
  
    public static void main(String args[]){ 
    	Exercise3 ob = new Exercise3(); //create new instance with array values
        double arr[] = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282}; //GIVEN ARRAY
        System.out.println("initial array: "); //print unsorted array
        ob.printArray(arr);
        ob.selection(arr);
        System.out.println("sorted array: "); //print sorted array
        ob.printArray(arr);
    }
}