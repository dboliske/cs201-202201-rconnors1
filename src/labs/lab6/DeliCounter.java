// Raymond Connors - CS201 - Section 02 - 03/22/22 - Exercise

import java.util.*;

public class DeliCounter {
	    
	    static Scanner input = new Scanner(System.in); //set up scanner
	    
	    public int addName(ArrayList<String> Queue){ //create method for first instance

	        System.out.print("Enter customer name: "); //prompt for customer name input
	        input.nextLine();
	        
	        String name = input.nextLine(); //store name input as string
	        
	        Queue.add(name); //add name to array
	        
	        return Queue.size(); //return customer position in line **will not print?**
	        
	    }
	    
	    public void helpCustomer(ArrayList<String> Queue){ //create method for second instance
	        
	        String Name = Queue.remove(0); //removes name of first customer in line
	        System.out.println(Name); //prints name of next customer to be assisted
	    }
	    
	    public void menu(ArrayList<String> Queue){ //create loop to present menu
	        
	        int input1 = 0;
	        
	        while(input1 != 3){ //create while loop that presents menu until 3 (exit) option is selected
	            
	            System.out.println("Select 1 to add customer name to queue");
	                System.out.println("Select 2 to present next customer in queue");
	                    System.out.println("Select 3 to exit program");
	            
	            input1 = input.nextInt();
	        
	            if(input1 == 1){ //instance if 1 is selected
	            addName(Queue);
	            
	            }
	            
	            else if(input1 == 2){ //instance if 2 is selected
	                helpCustomer(Queue);
	            }
	            
	            else if(input1 != 3){ //instance if invalid input is entered **works for integers, gives error if char is entered**
	                System.out.println("Invalid input");
	            }
	        
	        }
	    }
	    
	        public static void main(String[] args) {

	        ArrayList<String> customerQueue = new ArrayList<String>(); //creating array list

	        DeliCounter deli = new DeliCounter(); //creating instance of DeliCounter program
	        
	        deli.menu(customerQueue); //starting menu
	        
	    }
	}