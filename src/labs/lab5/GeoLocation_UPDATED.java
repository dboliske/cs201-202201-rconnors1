// Raymond Connors - CS201 - Section 02 - 03/07/22 - Exercise

package lab5;

public class GeoLocation_UPDATED { //declaration of class
	
	protected double latitude; //declaration of variables
	protected double longitude;
	
	public GeoLocation_UPDATED(){ //set variables
		latitude = 0;
		longitude = 0;
	}
	
	public GeoLocation_UPDATED(double latitude, double longitude){ //assigning variable types
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public double getLatitude() { //accessor method
		return latitude;
	}

	public void setLatitude(double latitude) { //mutator method
		this.latitude = latitude;
	}

	public double getLongitude() { //accessor method
		return longitude;
	}

	public void setLongitude(double longitude) { //mutator method
		this.longitude = longitude;
	}
	
	public String toString(){ //toString method
		return "Latitude: " + latitude + "Longitude : " + longitude; 
	}
	
	public boolean equals (GeoLocation_UPDATED g){ //equals operator (to check for equivalence between instances)
		if(Math.abs(g.getLatitude()-latitude)>0.001){ //check for latitude similarities
			return false;
		} else if (Math.abs(g.getLongitude()-longitude)>0.001){ //check for longitude similarities
			return false;
		}
		
		return true;
	}
	
	public double calcDistance(GeoLocation_UPDATED g){ //equation 1 **ADDED**
		return Math.sqrt(Math.pow(g.getLatitude()-latitude,2) + Math.pow(g.getLongitude()-longitude,2));
	}
	
	public double calcDistance(double latitude, double longitude){ //equation 2 **ADDED**
		return Math.sqrt(Math.pow(latitude-getLatitude(),2) + Math.pow(longitude-getLongitude(),2));
	}

}