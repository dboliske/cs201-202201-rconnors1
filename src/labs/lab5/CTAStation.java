// Raymond Connors - CS201 - Section 02 - 03/07/22 - Exercise

package lab5;

public class CTAStation extends GeoLocation_UPDATED{ //creation of the class
	
	protected String name; //variable declaration
	protected String location;
	protected boolean opened; 
	protected boolean wheelchair;
	protected double latitude;
	protected double longitude;

	
	public CTAStation(){ //initializing the variables
		super();
		name = "Name";
		location = "Location";
		opened = false;
		wheelchair = false;
	}
	
	public CTAStation(String name, String location, boolean opened, double latitude, double longitude, boolean wheelchair){ //assigning types
		this.name = name;
		this.location = location;
		this.opened = opened;
		this.latitude = latitude;
		this.longitude = longitude;
		this.wheelchair = wheelchair;
	}

	public String getName() { //accessor method
		return name;
	}

	public void setName(String name) { //mutator method
		this.name = name;
	}

	public String getLocation() { //accessor method
		return location;
	}

	public void setLocation(String location) { //mutator method
		this.location = location;
	}

	public boolean isOpened() { //accessor method
		return opened;
	}

	public void setOpened(boolean opened) { //mutator method
		this.opened = opened;
	}

	public double getLatitude() { //accessor method
		return latitude;
	}

	public void setLatitude(double latitude) { //mutator method
		this.latitude = latitude;
	}

	public double getLongitude() { //accessor method
		return longitude;
	}

	public void setLongitude(double longitude) { //mutator method
		this.longitude = longitude;
	}

	public boolean isWheelchair() { //accessor method
		return wheelchair;
	}

	public void setWheelchair(boolean wheelchair) { //mutator method
		this.wheelchair = wheelchair;
	}
	
	public String toString(){ //toString method
        return "CTA Station Information: Name: " + name + "Location: " + location + "Open: " + opened + "Latitude: " + latitude + "Longitude: " + longitude + "Wheelchair: " + wheelchair;
	}
	
	public boolean equals(GeoLocation_UPDATED g){ //equals method (to determine equivalence of contained values)
		if(((CTAStation)g).getLatitude()!=latitude){ //check for latitude similarities
			return false;
		} else if (((CTAStation)g).getLongitude()!=longitude){ //check for longitude similarities
			return false;
		}
		return true;
	}

	public double calcDistance(CTAStation c){ //equation 1 from goelocation
		return Math.sqrt(Math.pow(c.getLatitude()-latitude,2) + Math.pow(c.getLongitude()-longitude,2));
	}
	
	public double calcDistance(double latitude, double longitude){ //equation 2 from geolocation
		return Math.sqrt(Math.pow(latitude-getLatitude(),2) + Math.pow(longitude-getLongitude(),2));
	}	
}