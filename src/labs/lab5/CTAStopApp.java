// Raymond Connors - CS201 - Section 02 - 03/07/22 - Exercise

package lab5;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class CTAStopApp extends CTAStation //initializa class
{
    public static CTAStation[] cta_station = new CTAStation[34]; //create array (to be filled with contents of .csv file

    public static void displayNames(){ //instructions for displaying names of stations (if called)

        System.out.println("Station Names: ");

        for(int i=0; i<cta_station.length; i++){

            System.out.println(cta_station[i].name);

        }

    }

     

	public static void displayWheel(char ch){ //instructions for displaying station names on accessibility basis
		if(ch == 'y' || ch == 'Y'){
			System.out.println("Stations With Wheelchair Accessibility: ");
			boolean check = false;
			for (int i=0; i<cta_station.length; i++){
				if (cta_station[i].wheelchair == true){
					System.out.println(cta_station[i].name);
					check =true;
				}
			}
		} else {
			System.out.println("Stations Without Wheelchair Accessibility: ");
			boolean check = false;
			for (int i=0; i<cta_station.length; i++){
				if (cta_station[i].wheelchair == false){
					System.out.println(cta_station[i].name);
					check =true;
			}
		}  if (check ==false)
			System.out.println("No stations");
		}
	}
// ***something is not working correctly with this section of the code's execution, but I have been unable to figure out a solution ***
	
	
public void getDistance(double latitude, double longitude) //instructions for obtaining location and determining distances 

{

	GeoLocation_UPDATED c= new GeoLocation_UPDATED ();

	double distance=c.calDistance(latitude,longitude);

	for (int i=0; i<cta_station.length; i++)

	{

		if(c.equals(GeoLocation_UPDATED) == true)

			System.out.println(cta_station[i].name);

	}
}

    public static void main(String[] args){ //read data fro m.csv file (saved in documents folder)

        BufferedReader br = null;

        try{

            br = new BufferedReader(new FileReader("C:/Users/Meliora/Documents/CTAStops.csv")); //my specific save location of the file

            int i = 0;

         

            String line;

            while((line = br.readLine())!=null){ //fill array with corresponding data

                String[] temp = line.split(",");

                String name = temp[0];

                String location = temp[3];

                boolean opened = (temp[4].compareTo("TRUE")==0)? true:false;

                double latitude = Double.parseDouble(temp[1]);

                double longitude = Double.parseDouble(temp[2]);

                boolean wheelchair = (temp[5].compareTo("TRUE")==0)? true:false;

                CTAStation ct = new CTAStation(name,location,opened,latitude,longitude,wheelchair);

                cta_station[i] = ct;

                i += 1;

            }

            Scanner sc = new Scanner(System.in); //scanner for user input

            while(true){ //display menu

                System.out.println("Enter '1' To Display Station Names");

                System.out.println("Enter '2' To Display Wheelchair Access");

                System.out.println("Enter '3' To Display Distance To Each Station");

                System.out.println("Enter '4' To Exit");

                int n = sc.nextInt(); //select option, and carry out corresponding task

                if(n==1)

                    displayNames();

                else if(n==2){

                    System.out.print("Enter accessibility (y/n?): "); //prompts for wheelchair accessibility request

                    char ch = sc.next().charAt(0);

                    if (ch!= 'Y' || ch != 'y' || ch != 'n' || ch != 'N'){

                        ch = sc.next().charAt(0);

                    }

                    displayWheel(ch);

                }

                else if(n==3){
                	System.out.println("Enter Current Latitude, followed by Longitude: "); //prompts for input of location
                	int latitude = sc.nextInt();
                	int longitude = sc.nextInt();
                	//getDistance(latitude, longitude);
                }

                else break;

            }

        }

        catch (FileNotFoundException e){

            e.printStackTrace();

        }

        catch (IOException e){

            e.printStackTrace();

        }

    }

}