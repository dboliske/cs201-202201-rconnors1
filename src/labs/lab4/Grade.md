# Lab 4

## Total

24/30

## Break Down

* Part I GeoLocation
  * Exercise 1-9        6/8
  * Application Class   1/1
* Part II PhoneNumber
  * Exercise 1-9        6/8
  * Application Class   1/1
* Part III 
  * Exercise 1-8        6/8
  * Application Class   1/1
* Documentation         3/3

## Comments
1. Missing validLng(), have to check whether lat/lng are within valid bounds, not if the variable is a double.
2. Missing validAreaCode() and validNumber().
3. Missing validStrength().