// Raymond Connors - CS 201 - Final Project (General Store Inventory Management App)

package project;

import java.util.ArrayList;

public class Inventory { //creates inventory array
	
	private ArrayList<Item> stock; //create stock variable for items in inventory
	
	public Inventory() { //creating default constructor method
		stock = new ArrayList<Item>();
	}
	
	public Inventory(ArrayList<Item> stock) {
		this.stock = stock;
	}
	
	public ArrayList<Item> getStock() { //allows user to display entire stock
		return stock;
	}
	
	public void setStock(ArrayList<Item> stock) { //allows user to set entire stock
		this.stock = stock;
	}
	
	public void setStock(int index, Item i) { //allows for editing specific indices
		stock.set(index, i);
	}
	
	public Item getStock(int index) { //gets stock at specific indices
		return stock.get(index);
	}
	
	public String toString() { //toString method
		try {
		String storeStock = "";
		for (int i=0; i<stock.size(); i++) {
			storeStock += stock.get(i).toString() + "\n";
		}
		
		return storeStock;
		} catch(Exception e) {
			String error = "Error printing inventory"; //error catching for troubleshooting
			return error;
		}
	}
	
	public int size() { //gives inventory size
		return stock.size();
	}
	
	public void addItem(Item i) { //facilitates adding item to inventory
		stock.add(i);
	}
	
	public void removeItem(int index) { //facilitates removing item from inventory
		stock.remove(index);
	}
	
	public boolean equals(Object obj) { //checks for existing item
		if (obj == null) {
			return false;
		} else if (! (obj instanceof Inventory)) {
			return false;
		}
		
		Inventory i = (Inventory) obj;
		if (stock.size() != i.size()) {
			return false;
		}
		
		boolean same = true;
		for (int k=0; k<stock.size(); k++) {
			if (!stock.get(k).equals(i.getStock(k))) {
				same = false;
			}
		}
		
		if (same == false) {
			return false;
		}
		return true;
	}
	
	public void sortStock() { //SORTING
		for (int k=1; k<stock.size(); k++) { 
			int i = k;
			while (i > 0 && stock.get(i).getName().compareToIgnoreCase(stock.get(i-1).getName()) < 0) { 
				Item temp = stock.get(i-1);
				Item temp2 = stock.get(i);
				stock.set(i-1, temp2);
				stock.set(i, temp);
				i--; 
			}
		}
		
	}
	
	public int searchStock(String name, int start, int end) { //searches for specific stock in inventory by name
		if (start != end) {
			int middle = (start + (end))/2; 
			
			if (stock.get(middle).nameEquals(name)) { 
				return middle; 
			}
			
			if ((stock.get(middle).getName().compareToIgnoreCase(name)<0)) { 
				return searchStock(name, middle + 1, end); 
			}
			
			if ((stock.get(middle).getName().compareToIgnoreCase(name)>0)) { 
				return searchStock(name, start, middle); 
			}
		}
		return -1;
	}
	
	public double calcTotal() { //calculate total cart value
		double total = 0;
		for (int i=0; i<stock.size(); i++) {
			if (stock.get(i) != null) {
				total = total + stock.get(i).getPrice();
			}
		}
		int n = 2;
		total = Math.round(total*Math.pow(10,n))/Math.pow(10,n);
		return total;
	}
}