// Raymond Connors - CS 201 - Final Project (General Store Inventory Management App)

package project;

public class AgeRestriction extends Item { //facilitates adding/modifying items with an age restriction
	
	int age; //create variable for customer age required for purchase of restricted item
	
	public AgeRestriction() { //set default constructor values
		super();
		age = 0;
	}
	
	public AgeRestriction(String name, double price, int age) {
		super(name, price);
		setAge(age);
	}
	
	public int getAge() { //method for obtaining required age
		return age;
	}
	
	public void setAge(int age) { //method for setting required age
		if (age >= 0)
		this.age = age;
	}
	
	public String toString() { //toString method
		return super.toString() + "," + age;
	}
	
	public boolean equals(Object obj) { //checks for existing item
		if (!super.equals(obj)) { 
			return false;
		} else if (!(obj instanceof AgeRestriction)) { 
			return false;
		}
		
		AgeRestriction i = (AgeRestriction)obj; 
		if (this.age != i.getAge()) { 
			return false;
		}
		return true;
	}
	
	public String cartMessage() {
		return super.cartMessage() + "This item has an age restiction of " + age + " years"; //displays message on successfully adding item with age restriction to cart
	}
	
}
