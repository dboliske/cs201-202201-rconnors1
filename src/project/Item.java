// Raymond Connors - CS 201 - Final Project (General Store Inventory Management App)

package project;

public class Item { //class facilitates adding/managing of items in inventory

	private String name; //initialize variables
	private double price;
	
	public Item() { //set default values for constructor
		name = "none";
		price = 0;
	}
	
	public Item(String name, double price) {
		setName(name);
		setPrice(price);
	}
	
	public String getName() { //obtains item name
		return name;
	}
	
	public void setName(String name) { //sets item name
		this.name = name;
	}
	
	public double getPrice() { //obtains item price
		return price;
	}
	
	public void setPrice(double price) { //sets item price
		this.price = price;
	}
	
	public String toString() { //toString method
		return name + "," + price; 
	}
	
	public boolean equals(Object obj) { //checks for existing item
		if (!(obj instanceof Item)) { 
			return false;
		}
		
		Item i = (Item)obj; 
		if (!this.name.equals(i.getName())) { 
			return false;
		} else if (this.price != i.getPrice()) { 
			return false;
		}
		return true;
	}
	
	public boolean nameEquals(String s) { //checks for same name
		if (!this.name.equalsIgnoreCase(s)) { 
			return false;
		}
		return true;
	}
	
	public String cartMessage() {
		return name + " ($" + price + ") has been added to cart"; //message displayed when item successfully added to cart
	}
}
