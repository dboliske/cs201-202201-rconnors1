// Raymond Connors - CS 201 - Final Project (General Store Inventory Management App)

package project;

import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Store { //app that facilitates purchase & editing of inventory of a simulated shop
	
	public static void main(String[] args) { //loads file from memory location
		Scanner input = new Scanner(System.in);
		Inventory store = readFile("src/project/stock.csv");
		menu(store, input);
	}
	
	public static Inventory readFile(String filename) { //takes data from file and stores it in an accessible array
		Inventory store = new Inventory();
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);
			while(input.hasNextLine()) {
				String[] info = input.nextLine().split(",");
				if (info.length == 2) {
					Item i = new Item(info[0], Double.parseDouble(info[1]));
					store.addItem(i);
				} else {
					boolean parsing = true;
					try {
						Integer.parseInt(info[2]);
					} catch (Exception e) {
						parsing = false;
					}
					if (parsing == true) { //case of age-restricted item
						AgeRestriction a = new AgeRestriction(info[0], Double.parseDouble(info[1]), Integer.parseInt(info[2]));
						store.addItem(a);
					} else { //case of produce item
						Produce p = new Produce(info[0], Double.parseDouble(info[1]), (info[2]));
						store.addItem(p);
					}
				}
				
			}
			store.sortStock();
			input.close();
		} catch (Exception e) {
			System.out.println("Error reading file.");
		}
		return store;
	}
	

	public static void writeFile(Inventory store) { //writes new array data to csv format
		store.sortStock();
		try {
			FileWriter fw = new FileWriter("src/project/stock.csv"); //same location, since same file is being saved to
			
			for (int i=0; i<store.size(); i++) {
				fw.write(store.getStock(i).toString() + "\n");
				fw.flush();
			}
			
			fw.close();
			
		} catch (Exception e) {
			System.out.println("Error writing file.");
		}
	}
	
	public static void menu(Inventory store, Scanner input) { //menu prompt for initial user input
		boolean done = false;
		do {
			System.out.println("Welcome to the store!");
			System.out.println("Please select which action you would like, by entering the corresponding number below!");
			System.out.println("");
			System.out.println("1. List Current Store Inventory");
			System.out.println("2. Sell Item");
			System.out.println("3. Inventory Search");
			System.out.println("4. Modify Existing Stock");
			System.out.println("5. Add Item To Stock");
			System.out.println("6. Save and Exit");
		
			String choice = input.nextLine();
			switch (choice) {
				case "1": 
					printStock(store);
					break;
				case "2":
					removeItem(store, input);
					break;
				case "3":
					searchStore(store, input);
					break;
				case "4":
					modifyStock(store, input);
					break;
				case "5":
					addItem(store, input);
					break;
				case "6":
					done = true;
					writeFile(store);
					break;
				default:
					System.out.println("Invalid Entry");
			}
		} while (!done);
	}
	
	public static double userPricing(Scanner input) { //input price from user
		double price = -1;
		do {
			System.out.print("Enter price: ");
			try {
				price = Double.parseDouble(input.nextLine());
			} catch (Exception e) {
				System.out.println("Price must be a positive number");
			}
		} while (price < 0);
		return price;
	}
	
	public static String userDate(Scanner input) { //gets expiration date (FORMATTING)
		String datePattern = "([13578]|[0][13578]|[1][02])(-|/)"+"([1-9]|[0][1-9]|[12][0-9]|[3][01])(-|/)"+"([0-9]+)";
		String datePattern2 = "([469]|[0][469]|[1][1])(-|/)"+"([1-9]|[0][1-9]|[12][0-9]|[3][0])(-|/)"+"([0-9]+)";
		String datePattern3 = "([2]|[0][2])(-|/)"+"([1-9]|[0][1-9]|[1][0-9]|[2][0-8])(-|/)"+"([0-9]+)";
		boolean done = false;
		String expiration = "";
		do {
			System.out.print("Enter expiration date: ");
			expiration = input.nextLine();
			if (Pattern.matches(datePattern, expiration) || Pattern.matches(datePattern2, expiration) || Pattern.matches(datePattern3, expiration)) {
				done = true;
			} else {
				System.out.println("Please enter date in MM/DD/YYYY format");
			}
			} while (!done);
		return expiration;
	}
	
	public static int userAge(Scanner input) { //gets age for restricted items
		int age = -1;
		do {
			System.out.print("Enter age to be restricted: ");
			try {
				age = Integer.parseInt(input.nextLine());
			} catch (Exception e) {
				System.out.println("Entered age must be an integer");
			}
		} while (age < 0);
		return age;
	}
	
	public static Inventory addItem(Inventory store, Scanner input) { //Allows user to select type of item to be added
		System.out.println("Enter 1 to create an item with no restictions or expirations");
		System.out.println("Enter 2 to create a produce item");
		System.out.println("Enter 3 to create an age restricted item");
		String choice = input.nextLine();
		switch (choice) {
		case "1": //case for adding normal item with no restrictions
			System.out.print("Enter item name: ");
			String name = input.nextLine();
			double price = userPricing(input);
			Item i = new Item(name, price);
			store.addItem(i);
			break;
		case "2": //case for adding produce item with expiration date
			System.out.print("Enter item name: ");
			String name1 = input.nextLine();
			double price1 = userPricing(input);
			String expiration = userDate(input);
			Produce p = new Produce(name1, price1, expiration);
			store.addItem(p);
			break;
		case "3": //case for adding item with age restriction
			System.out.print("Enter item name: ");
			String name2 = input.nextLine();
			double price2 = userPricing(input);
			int age = userAge(input);
			AgeRestriction a = new AgeRestriction(name2, price2, age);
			store.addItem(a);
			break;
		default: 
			System.out.println("Invalid input");	
		}
		
		return store;
	}
	
	public static Inventory removeItem(Inventory store, Scanner input) { //removes items from inventory (adds to cart)
		Inventory cart = new Inventory();
		boolean done = false;
		do {
			System.out.print("Enter name of desired item: "); //prompts user for desired item to remove/add to cart
			String choice = input.nextLine();
			store.sortStock();
			int index = store.searchStock(choice, 0, store.size());
			if (index == -1) {
				System.out.println("Item not found");
			} else {
				System.out.println("Item price: $" + store.getStock(index).getPrice());
				System.out.println("Enter y/n to add item to cart or return to inventory");
				String yn = input.nextLine();
				switch (yn.toLowerCase()) {
				case "y": //case to add item to cart
					cart.addItem(store.getStock(index)); 
					System.out.println(store.getStock(index).cartMessage()); 
					store.removeItem(index);
					break;
				case "n": //case to return item to inventory
					System.out.println("Item was not added to cart"); 
					break;
				default:
					System.out.println("Invalid input");
				}
			} 
			System.out.println("Check out? (y to check out, n to continue shopping)");
				String yn = input.nextLine();
				switch (yn.toLowerCase()) {
				case "y": //case to check out cart
					done = true;
					break;
				} 
		} while (!done);	
		
		boolean done1 = false;
		do {
			System.out.println("Ready to check out? (y to continue, n to remove item from cart)");
			String cartDecision = input.nextLine();
			switch (cartDecision) {
			case "n": //allows user to remove items from cart prior to check out
				System.out.println("Enter item to remove from cart: ");
				String item = input.nextLine();
				cart.sortStock();
				int index = cart.searchStock(item, 0, cart.size()); //remove item from cart if found via search
				if (index == -1) {
					System.out.println("Item not in cart");
				} else {
					store.addItem(cart.getStock(index));
					cart.removeItem(index);
					System.out.println(item + " is no longer in the cart");
				}
				break;
			case "y":
				done1 = true;
				break;
			default:
				System.out.println("Invalid entry");
			}
		} while (!done1);

			System.out.println("Total is: $" + cart.calcTotal());
		return store;	
	}
	
	public static Inventory modifyStock(Inventory store, Scanner input) { //case for modification of existing item
		// search for item
		System.out.print("Enter item to be modified ");
		String item = input.nextLine();
		store.sortStock();
		int index = store.searchStock(item, 0, store.size());
		if (index == -1) {
			System.out.println("Item not in inventory");
		} else {
			if ((store.getStock(index) instanceof Item) && !(store.getStock(index) instanceof Produce) && !(store.getStock(index) instanceof AgeRestriction)) { //iteration to edit multiple instances of same item
				System.out.print("Enter the new item name: ");
				String name = input.nextLine();
				double price = userPricing(input);
				for (int i=0; i<store.size(); i++) {
					if ((store.getStock(i).nameEquals(item)) && (store.getStock(i) instanceof Item)) {
						store.getStock(i).setName(name);
						store.getStock(i).setPrice(price);
					}
				}
			} else if (store.getStock(index) instanceof Produce) { //case for modification of produce item
				System.out.print("Enter the new item name: ");
				String name = input.nextLine();
				double price = userPricing(input);
				String expiration = userDate(input);
				Produce p = new Produce(name, price, expiration);
				int position = 0;
				int counter = 0;
				do {
					position = store.searchStock(item, 0, store.size()); //iterate to find and edit multiple instances of the item to be modified
					if (position != -1) {
						store.removeItem(position);
						counter++;
					}
				} while (position != -1);
				for (int k=0; k<counter; k++) {
					store.addItem(p); // add the same amount of the new produce item to the store
				}
			} else if (store.getStock(index) instanceof AgeRestriction) { //case for modification of age-restricted item
				System.out.print("Enter the new item name: ");
				String name = input.nextLine();
				double price = userPricing(input);
				int age = userAge(input);
				AgeRestriction a = new AgeRestriction(name, price, age);
				int position = 0;
				int counter = 0;
				do {
					position = store.searchStock(item, 0, store.size()); //iterate to find and edit multiple instances of the item to be modified
					if (position != -1) {
						store.removeItem(position);
						counter++;
					}
				} while (position != -1);
				for (int k=0; k<counter; k++) {
					store.addItem(a);
				}
			}
		}
		store.sortStock(); //SORT AFTER MODIFICATION COMPLETE
		return store;
	}
	
	public static void searchStore(Inventory store, Scanner input) { //case for searching store inventory for specific item
		System.out.print("Enter the name of item to be searched: ");
		String word = input.nextLine();
		store.sortStock();
		int index = store.searchStock(word, 0, store.size());
		if (index == -1) {
			System.out.println("Item not found in store inventory");
		} else {
			System.out.println("Your item is located at " + index);
		}
	}
	
	public static void printStock(Inventory store) { //prints inventory stock
		System.out.println(store);
	}
}