// Raymond Connors - CS 201 - Final Project (General Store Inventory Management App) 

package project;

//import java.util.regex.Pattern;

public class Produce extends Item { //facilitates adding/modifying produce item in inventory
	
	String expirationDate; //initialize expiration date variable
	
	//private static String datePattern = "([13578]|[0][13578]|[1][02])(-|/)"+"([1-9]|[0][1-9]|[12][0-9]|[3][01])(-|/)"+"([0-9]+)";
	//private static String datePattern2 = "([469]|[0][469]|[1][1])(-|/)"+"([1-9]|[0][1-9]|[12][0-9]|[3][0])(-|/)"+"([0-9]+)";
	//private static String datePattern3 = "([2]|[0][2])(-|/)"+"([1-9]|[0][1-9]|[1][0-9]|[2][0-8])(-|/)"+"([0-9]+)";
	
	public Produce() { //set default constructor
		super();
		expirationDate = "01/01/2022";
	}
	
	public Produce(String name, double price, String exp) {
		super(name, price);
		expirationDate = "01/01/2022";
		setExpiration(exp);
	}
	
	public String getExpiration() { //method for obtaining expiration date
		return expirationDate;
	}
	
	public void setExpiration(String expiration) { //method for setting expiration date
		//if (Pattern.matches(datePattern, expiration) || Pattern.matches(datePattern2, expiration) || Pattern.matches(datePattern3, expiration)) {
			this.expirationDate = expiration;
		}
	//}

	public String toString() { //toString method
		return super.toString() + "," + expirationDate;
	}
	
	public boolean equals(Object obj) { //checks for existing item
		if (!super.equals(obj)) { 
			return false;
		} else if (!(obj instanceof Produce)) { 
			return false;
		}
		
		Produce i = (Produce)obj; 
		if (!this.expirationDate.equals(i.getExpiration())) { 
			return false;
		}
		return true;
	}
	
	public String cartMessage() {
		return super.cartMessage() + "Item Expiration Date: " + expirationDate; //message displayed when produce item successfully added to cart
	}
		
}
